class Sample_Class() :

	def __init__(self, year) :
		self.year = year

	def show_year(self):
		print(f"The year is: {self.year}.")

sample_obj = Sample_Class(2023)

print(sample_obj.year)
sample_obj.show_year()

# Coupling and De-coupling
# Coupling: when classes are dependent on one another (ex. parent class and child class; what functions a child class inherits depends on the parent, and will be impacted if any changes are done on the parent class)
# De-coupling: when classes are independent of one another (ex. two separate classes; even if class A's functions are modified, nothing will happen to class B's functions)

# [Section] Fundamentals of OOP in Python
# The Four Pillars of OOP: Encapsulation, Inheritance, Polymorphism, Abstraction

# 1. Encapsulation: mechanism of wrapping the attributes and codes acting on the methods together as a single unit
	# In encapsulation, the attributes of a class will be hidden from other classes, and can be accessed only through the methods of their current class (data hiding)
	# Setter: sets the attributes of the new class; getter : accesses these set attributes or properties

class Person() :

	# properties
	def __init__(self, name, age) :
		self._name = name
		self._age = age

	# methods
	# getter and setter for _name attribute
	def get_name(self) :
		print(f"Name of person: {self._name}")

	def set_name(self, name) :
		self._name = name

	# getter and setter for _age attribute
	def get_age(self) :
		print(f"Age of person: {self._age}")

	def set_age(self, age) :
		self._age = age

# new instance
person_one = Person("James", 245)

# print(person_one._name) / Ideally you shouldn't be able to just access the object's data like this unless it's through the object's methods.
person_one.get_name()

person_one.set_name("Kat")

person_one.get_name()

person_one.get_age()
person_one.set_age(3)
person_one.get_age()

# 2. Inheritance: the inheriting of a characteristic or attribute from a parent class to a child class
# Syntax:
	# class Child_Class_Name(Parent_Class_Name) :

class Employee(Person) :

	# attributes
	def __init__(self, name, age, employee_id) :
		# super() can be used to invoke immediate parent class instructors
		super().__init__(name, age)

		self._employee_id = employee_id

	# moethids
	def get_id(self) :
		print(f"Employee ID: {self._employee_id}")

	def set_id(self, employee_id) :
		self._employee_id = employee_id

employee_one = Employee("CongTV", 27, "0001")

employee_one.get_age()
employee_one.set_age(28)
employee_one.get_age()

employee_one.get_name()
employee_one.set_name("Lincoln")
employee_one.get_name()

employee_one.get_id()
employee_one.set_id("Legend")
employee_one.get_id()


# 3. Polymorphism: checking that classes have the same functions (but not necessarily return the same result) 
# With Functions and Objects
class Team_Lead() :
	def occupation(self) :
		print("Team Lead")

	def has_auth(self) :
		print(True)

class Team_Member() :
	def occupation (self) :
		print("Team Member")

	def has_auth(self) :
		print(False)

team_lead = Team_Lead()
team_member = Team_Member()

for person in (team_lead, team_member) :
	person.occupation()

# With Inheritance
# Recall that child classes inherit methods from their parent classes.

class Zuitt() :
	def tracks(self) :
		print("We are currently offering three tracks: developer career, pi-shape career, and short courses.")

	def num_of_hours(self) :
		print("Learn web development in 360 hours!")

class Developer_Career(Zuitt) :
	# Overriding of the parent method
	def num_of_hours(self) :
		print("Learn the basics of web dev in 240 hours!")

class Pi_Shape_Career(Zuitt) :
	def num_of_hours(self) :
		print("Learn no-code app dev in 140 hours!")

class Short_Courses(Zuitt) :
	def num_of_hours(self) :
		print("Learn advanced web dev topics in 20 hours!")

course = Zuitt()
course_one = Developer_Career()
course_two = Pi_Shape_Career()
course_three = Short_Courses()

course.tracks()
course.num_of_hours()
for course in (course_one, course_two, course_three) :
	course.num_of_hours()

# 4. Abstraction: an abstract class can be considered as a blueprint for other classes

from abc import ABC
# This tells the program to import the ABC module of Python for this code's use.

class Polygon(ABC) :
	def print_number_of_sides(self) :
		# The pass keyword indicates that the method doesn't do anything
		pass

class Triangle(Polygon) :
	def __init__(self) :
		super().__init__()

	def print_number_of_sides(self) :
		print("This polygon has 3 sides.")

class Pentagon(Polygon) :
	def __init__(self) :
		super().__init__()
		
	def print_number_of_sides(self) :
		print("This polygon has 5 sides.")

shape_one = Triangle()
shape_two = Pentagon()

shape_one.print_number_of_sides()
shape_two.print_number_of_sides()