from abc import ABC

class Animal(ABC) :

	# Properties
	def __init__(self, name, breed, age) :
		self._name = name
		self._breed = breed
		self._age = age

	# Methods
	def eat(self, food) :
		print(f"You fed {self._name} some {food}!")

	def make_sound(self) :
		pass

	def call(self) :
		pass

	# Getters
	def get_name(self) :
		print(f"This pet's name is {self._name}.")

	def get_breed(self) :
		print(f"This pet's breed is {self._breed}.")

	def get_age(self) :
		print(f"This pet is {self._age} years old.")

	# Setters
	def set_name(self, name) :
		self._name = name

	def set_breed(self, breed) :
		self._breed = breed

	def set_age(self, age) :
		self._age = age		

class Dog(Animal) :

	# Properties
	def __init__ (self, name, breed, age) :
		super().__init__(name, breed, age)

	# Methods
	def make_sound(self) :
		print("Bark! Bark bark bark!")

	def call(self) :
		print(f"Here, {self._name}!")

class Cat(Animal) :

	# Properties
	def __init__ (self, name, breed, age) :
		super().__init__(name, breed, age)

	# Methods
	def make_sound(self) :
		print("Meow? Mrooowr...")

	def call(self) :
		print(f"{self._name}, come!")


# Test Cases
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

# Commented out getter and setter tests; uncomment to test

# dog1.get_name()
# dog1.get_breed()
# dog1.get_age()

# dog1.set_name("Koromaru")
# dog1.get_name()
# dog1.set_breed("Shiba Inu")
# dog1.get_breed()
# dog1.set_age(6)
# dog1.get_age()

# cat1.get_name()
# cat1.get_breed()
# cat1.get_age()

# cat1.set_name("Morgana")
# cat1.get_name()
# cat1.set_breed("Black Stray")
# cat1.get_breed()
# cat1.set_age(14)
# cat1.get_age()